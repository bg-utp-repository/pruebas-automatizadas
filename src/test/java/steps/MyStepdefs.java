package steps;

import hellocucumber.StepDefinitions;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;

public class MyStepdefs extends StepDefinitions {
    @Dado("que el usuario ingresa a {string}")
    public void queElUsuarioIngresaA(String arg0) {
        assembleLink(arg0);
    }

    @Cuando("ingresa en el input de búsqueda {string}")
    public void ingresaEnElInputDeBúsqueda(String arg0) {
        searchInput(arg0);
    }

    @Y("selecciona el botón {string}")
    public void seleccionaElBotón(String arg0) {
        clickOnButton(arg0);
    }

    @Entonces("Se muestra la dirección {string}")
    public void seMuestraLaDirección(String arg0) {
        searchResult(arg0);
    }
}
