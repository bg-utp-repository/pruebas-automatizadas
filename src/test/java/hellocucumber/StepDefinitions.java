package hellocucumber;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static hellocucumber.Environment.osDetected;

public class StepDefinitions {
    public static WebDriver driver;
    public static WebDriverWait wait;

    public void assembleLink(String requestLink) {
//        System.setProperty("webdriver.chrome.driver", osDetected);
        System.setProperty("webdriver.chrome.driver", "src/test/resources/attachments/linux/chromedriver");
        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.get(requestLink);
        driver.manage().window().maximize();
    }

    public void searchInput(String search) {
        wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@class='gLFyf gsfi']"))));
        driver.findElement(By.xpath("//*[@class='gLFyf gsfi']")).sendKeys(search);
    }

    public void clickOnButton(String button) {
        if (button.equalsIgnoreCase("Buscar en Google")) {
            wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@jsname='VlcLAe']/center/input[1]"))));
            driver.findElement(By.xpath("//*[@jsname='VlcLAe']/center/input[1]")).click();
        } else {
            System.out.println("El botón definido no ha sido mapeado");
        }
    }

    public void searchResult(String result) {
        WebElement element = driver.findElement(By.xpath("//*[@class='BYM4Nd']/div/div/link"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        Assert.assertEquals(element.getAttribute("href"), (result));
        System.out.println("Se muestra correctamente la dirección de " + element.getAttribute("href"));
    }
}
