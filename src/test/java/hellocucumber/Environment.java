package hellocucumber;

public class Environment {
    static String osName = System.getProperty("os.name").toLowerCase();
    static String osDetected = System.getProperty("user.dir") + "/src/test/resources/attachments/" + osName + chromedriver();//abrir chromedriver

    public static String chromedriver() {
        return (osName.contains("windows")) ? "/chromedriver.exe" : "/chromedriver";
    }
}
