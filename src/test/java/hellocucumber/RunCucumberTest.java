package hellocucumber;


import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.AfterMethod;

import static hellocucumber.StepDefinitions.*;


@CucumberOptions(features = "src/test/java/hellocucumber",
        tags = "(@wip)",
        glue = "steps",
        plugin = {"summary", "pretty"},
        snippets = CucumberOptions.SnippetType.CAMELCASE)

public class RunCucumberTest extends AbstractTestNGCucumberTests {

//    @AfterMethod
    public void closeAll() {
        try {
            driver.close();
            System.out.println("Se cerro correctamente el explorador ");
        } catch (Exception e) {
            System.out.println("No se pudo cerrar chrome " + e.getMessage());
            throw new IllegalStateException(e);
        }
    }
}
